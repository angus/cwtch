package connections

import (
	"crypto/rsa"
	"cwtch.im/cwtch/model"
	"cwtch.im/cwtch/peer/peer"
	"cwtch.im/cwtch/protocol"
	"git.openprivacy.ca/openprivacy/libricochet-go"
	"git.openprivacy.ca/openprivacy/libricochet-go/channels"
	"git.openprivacy.ca/openprivacy/libricochet-go/connection"
	"git.openprivacy.ca/openprivacy/libricochet-go/identity"
	"git.openprivacy.ca/openprivacy/libricochet-go/utils"
	"net"
	"testing"
	"time"
)

func PeerAuthValid(string, rsa.PublicKey) (allowed, known bool) {
	return true, true
}

func runtestpeer(t *testing.T, tp *TestPeer, privateKey *rsa.PrivateKey) {
	ln, _ := net.Listen("tcp", "127.0.0.1:5452")
	conn, _ := ln.Accept()
	defer conn.Close()

	rc, err := goricochet.NegotiateVersionInbound(conn)
	if err != nil {
		t.Errorf("Negotiate Version Error: %v", err)
	}
	rc.TraceLog(true)
	err = connection.HandleInboundConnection(rc).ProcessAuthAsServer(identity.Initialize("", privateKey), PeerAuthValid)
	if err != nil {
		t.Errorf("ServerAuth Error: %v", err)
	}
	tp.RegisterChannelHandler("im.cwtch.peer", func() channels.Handler {
		cpc := new(peer.CwtchPeerChannel)
		cpc.Handler = tp
		return cpc
	})

	go func() {
		alice := model.GenerateNewProfile("alice")
		time.Sleep(time.Second * 1)
		rc.Do(func() error {
			channel := rc.Channel("im.cwtch.peer", channels.Inbound)
			if channel != nil {
				peerchannel, ok := channel.Handler.(*peer.CwtchPeerChannel)
				if ok {
					peerchannel.SendMessage(alice.GetCwtchIdentityPacket())
				}
			}
			return nil
		})
	}()

	rc.Process(tp)
}

type TestPeer struct {
	connection.AutoConnectionHandler
	ReceivedIdentityPacket bool
	ReceivedGroupInvite    bool
}

func (tp *TestPeer) ClientIdentity(ci *protocol.CwtchIdentity) {
	tp.ReceivedIdentityPacket = true
}

func (tp *TestPeer) HandleGroupInvite(gci *protocol.GroupChatInvite) {
	tp.ReceivedGroupInvite = true
}

func (tp *TestPeer) GetClientIdentityPacket() []byte {
	return nil
}

func TestPeerPeerConnection(t *testing.T) {
	privateKey, err := utils.GeneratePrivateKey()
	if err != nil {
		t.Errorf("Private Key Error %v", err)
	}
	onionAddr, err := utils.GetOnionAddress(privateKey)
	if err != nil {
		t.Errorf("Onion address error %v", err)
	}

	profile := model.GenerateNewProfile("alice")
	ppc := NewPeerPeerConnection("127.0.0.1:5452|"+onionAddr, profile)
	//numcalls := 0
	tp := new(TestPeer)
	tp.Init()
	go runtestpeer(t, tp, privateKey)
	state := ppc.GetState()
	if state != DISCONNECTED {
		t.Errorf("new connections should start in disconnected state")
	}
	go ppc.Run()
	time.Sleep(time.Second * 5)
	state = ppc.GetState()
	if state != AUTHENTICATED {
		t.Errorf("connection state should be authenticated(3), was instead %v", state)
	}

	if tp.ReceivedIdentityPacket == false {
		t.Errorf("should have received an identity packet")
	}

	_, invite, _ := profile.StartGroup("aaa.onion")
	ppc.SendGroupInvite(invite)
	time.Sleep(time.Second * 3)
	if tp.ReceivedGroupInvite == false {
		t.Errorf("should have received an group invite packet")
	}

}
